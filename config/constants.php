<?php
return [
    'USES_TYPES' =>['attack_damage', 'attack_speed','armor', 'magic_resist',
    'spell_damage','starting_mana','health','special','gloves' ],

    'RATEDS'=>['s','a','b','c','d','e'],

    'STATUS_CHAMPIONS'=>['buff','neft','adjust','normal'],

    'FILE_TYPES' => ['file', 'image'],
];