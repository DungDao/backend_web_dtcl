<?php

return [
    'credentials' => 'true',

    'origins' => [
        '*'
    ],

    'methods' => [
        'POST',
        'GET',
        'OPTIONS',
        'PUT',
        'PATCH',
        'DELETE',
    ],

    'headers' => [
        'Origin', 
        'Content-Type', 
        'X-Auth-Token', 
        'Authorization', 
        'X-Requested-With', 
        'x-xsrf-token'
    ]
];