<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('email')->unique();
            $table->tinyInteger('roles')->default(0);
            $table->string('avatar')->nullable();
            $table->string('phone',20)->nullable();
            $table->tinyInteger('remeber_user')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('block_status')->default(0);
            $table->string('password');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
