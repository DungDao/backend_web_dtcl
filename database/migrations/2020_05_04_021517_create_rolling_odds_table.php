<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRollingOddsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rolling_odds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('level')->unique();
            $table->integer('tier1')->nullable();
            $table->integer('tier2')->nullable();
            $table->integer('tier3')->nullable();
            $table->integer('tier4')->nullable();
            $table->integer('tier5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rolling_odds');
    }
}
