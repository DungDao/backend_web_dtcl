<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseOfAdvancedItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_of_advanced_item', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('advanced_item_id');
            $table->unsignedBigInteger('base_item_id');
            // $table->primary(['advanced_item_id','base_item_id'],'base_of_advanced_item_id');

            $table->foreign('advanced_item_id')->references('id')->on('advanced_items')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('base_item_id')->references('id')->on('base_items')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_of_advanced_item');
    }
}
