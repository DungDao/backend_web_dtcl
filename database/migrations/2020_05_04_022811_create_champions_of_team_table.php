<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChampionsOfTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champions_of_team', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('team_composition_id');
            $table->unsignedBigInteger('champion_id');
            $table->tinyInteger('three_stars')->default(0);
            $table->integer('position');
            $table->bigInteger('item1')->nullable();
            $table->bigInteger('item2')->nullable();
            $table->bigInteger('item3')->nullable();
            $table->foreign('team_composition_id')->references('id')->on('team_composition')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('champion_id')->references('id')->on('champions')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champions_of_team');
    }
}
