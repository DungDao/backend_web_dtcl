<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChampionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('avatar')->nullable();
            $table->string('mana');
            $table->integer('price');
            $table->string('health');
            $table->string('start_mana');
            $table->integer('range');
            $table->string('attack_damage');
            $table->double('attack_speed');
            $table->integer('armor');
            $table->string('dps');
            $table->integer('magic_resist');
            $table->text('skill');
            $table->text('info_skill');
            $table->string('avatar_skill');
            $table->enum('rated', config('constants.RATEDS'));
            $table->enum('status', config('constants.STATUS_CHAMPIONS'))->default('normal');
            $table->text('experience')->nullable();
            $table->text('info_update')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champions');
    }
}
