<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChampionsByClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champions_by_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('champion_id')->index();
            $table->unsignedBigInteger('classes_id')->index();

            $table->foreign('champion_id')->references('id')->on('champions')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('classes_id')->references('id')->on('classes')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champions_by_classes');
    }
}
