<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsOfChamionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_of_chamions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('champion_id');
            // $table->primary(['item_id','champion_id'],'items_of_chapion_id');

            $table->foreign('item_id')->references('id')->on('advanced_items')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('champion_id')->references('id')->on('champions')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_of_chamions');
    }
}
