@component('mail::message')
<h3> Quên mật khẩu !</h3><br>
Dear, {{$name}}<br>

Bạn nhận được email này vì chúng tôi đã nhận được yêu cầu đặt lại mật khẩu cho tài khoản của bạn.

<a href="{{$url}}">Tạo mới mật khẩu</a>


<br>
<br>
Nếu bạn không yêu cầu đặt lại mật khẩu. Vui lòng bỏ qua email này hoặc trả lời cho chúng tôi biết.
<br>
<br>
Cảm ơn,<br>
TFT24h support team
{{-- {{ config('app.name') }} --}}
@endcomponent