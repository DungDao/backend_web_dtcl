<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes 
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('login-home', 'AuthController@loginHome');
    Route::post('signup', 'AuthController@signup');
  	Route::post('reset-password', 'ResetPasswordController@sendMail');
	Route::put('reset-password/{token}', 'ResetPasswordController@reset');
	
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'Common',
    'prefix' => 'common'
],function() {
    Route::post('upload', 'FileController@upload');
    Route::post('upload-image-editor','FileController@uploadUrlEditor');
});

Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'Admin',
    'prefix' => 'admin'
],function() {
    Route::apiResource('classes', 'ClassController');
    Route::get('class/get-all-classes','ClassController@getAll');

    Route::apiResource('post','PostController');
    Route::apiResource('post-category','CategoryPostController');
    Route::get('category-post-all','CategoryPostController@getAll');
    Route::put('posts/posted/{id}','PostController@posted');
    Route::get('get-posts-by-category/{id}','PostController@getPostByCategory');

    Route::apiResource('combination-rate','CombinationRateController');

    Route::apiResource('champion','ChampionController');
    Route::get('champion-all','ChampionController@getAll');

    Route::apiResource('base-item','BaseItemController');
    Route::get('base-item-all','BaseItemController@getAll');

    Route::apiResource('advanced-item','AdvancedItemController');
    Route::get('advanced-item-all','AdvancedItemController@getAll');

    Route::apiResource('base-of-advanced-item','BaseOfAvancedItemController');
    Route::apiResource('team','TeamCompositionController');

    Route::get('user/get-users','UserController@getUsers');
    Route::put('user/block-status/{id}','UserController@blockUser');

    Route::apiResource('utilities','UtilityController');
    Route::get('utility/get-by-type','UtilityController@getByType');

    Route::apiResource('rolling-odd','RollingOddController');

    Route::get('home/get-total-home','HomeController@getTotalInTable');

    Route::get('home/get-number-user-by-date','HomeController@getUserByCreateAt');

    Route::get('home/get-number-post-by-date','HomeController@getNumberPostByDate');

});

Route::group([
    'namespace' => 'Home',
    'prefix' => 'home'
],function() {
    Route::get('list-champions', 'HomeController@getAllChampions');
    Route::get('all-champions','HomeController@getChampions');
    Route::get('champions/{id}','HomeController@getChampionById');
    Route::get('list-items', 'HomeController@getAllItems');
    Route::get('list-base-items', 'HomeController@getAllBaseItems');
    Route::get('list-classes', 'HomeController@getAllClass');
    Route::get('list-classes-with-champions', 'HomeController@getAllClassWithChampions');
    
    // Post Frontend
    Route::get('get-post-by-cate-id/{id}','PostController@getPostByCategory');
    Route::get('get-post-new','PostController@getPostNew');
    Route::get('get-post-popu','PostController@getPostPopu');
    Route::get('get-post/{id}','PostController@getPostById');
    Route::put('watch-post/{id}','PostController@watchPost');
    Route::get('get-teams','HomeController@getTeams');
    Route::get('get-categories','PostController@getAllCategory');

    // Get Info by Rated
    // 
    Route::get('get-champion-by-rated','HomeController@getChampionByRated');
    Route::get('get-utilities-by-type','HomeController@getByTypeUtitlitis');
    Route::get('get-rollings','HomeController@getRollings');

    Route::apiResource('comment','CommentController');
    Route::apiResource('team','TeamController');
    Route::get('team-by-user/{id}','TeamController@getTeamByIdUser');
    Route::post('add-comment-team','HomeController@addCommentTeams');
    // User 
    // 
    Route::get('get-user-by-id/{id}','HomeController@getUserById');
    Route::put('change-info-user/{id}','HomeController@changInfoUser');

});