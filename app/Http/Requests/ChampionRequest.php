<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChampionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'avatar' => 'required',
            'mana' => 'required',
            'price'=>'required',
            'health'=>'required',
            'start_mana'=>'required',
            'range'=>'required',
            'attack_damage'=>'required',
            'attack_speed'=>'required',
            'armor'=>'required',
            'dps'=>'required',
            'magic_resist'=>'required',
            'skill'=>'required',
            'avatar_skill'=>'required',
            'rated'=>'required',
            'status'=>'required',
        ];
    }
}
