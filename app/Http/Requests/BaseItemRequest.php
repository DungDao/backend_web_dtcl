<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'avatar' => 'required',
            'uses_type' => 'required',
            'number_power'=>'required',
            'rated'=>'required',
        ];
    }
}
