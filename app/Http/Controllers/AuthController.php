<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController as BaseController;
use Carbon\Carbon;
use App\User;
use Validator;
 
class AuthController extends BaseController
{
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:users',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'roles'=>'required',
        ]);
 
         if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'roles'=>$request->roles,
            'avatar'=>$request->avatar
        ]);
 
        $user->save();
 
        return $this->sendResponse($user,"Add Account Cuccessfuly");
    }
 
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
 
        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors()->toArray(),
            ]);
        }
 
        $credentials = request(['email', 'password']);
 
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 'fails',
                'message' => 'Tài khoản của bạn không chính xác'
            ], 401);
        }
 
        $user = $request->user();
        if($user->roles==0){
            return response()->json([
                'status' => 'fails',
                'message' => 'Tài khoản của bạn không có quyền hạn quản lý'
            ], 401);
        }else{
            $user->password=$request->password;
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            
            $remeber_me=null;
            if ($request->remeber_user==1) {
                $token->expires_at = Carbon::now()->addWeeks(1);
                $remeber_me=1;
            }else{
                $remeber_me=0;
            }
     
            $token->save();
        }
       
        return response()->json([
            'status' => 'success',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'code'=>200,
            'password'=>$request->password,
            'remeber'=>$request->remeber_user,
            'data'=>$user,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
 
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => 'success',
            'code'=>200,
            'message'=>"logout successfuly"
        ]);

        return $this->sendResponse("Logout Successfuly");

    }
 
    public function user(Request $request)
    {
        return $this->sendResponse($request->user(),"Show Info User Successfuly");
    }

     public function loginHome(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
 
        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors()->toArray(),
            ]);
        }
 
        $credentials = request(['email', 'password']);
 
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 'fails',
                'message' => 'Tài khoản của bạn không chính xác'
            ], 401);
        }
 
        $user = $request->user();
            $user->password=$request->password;
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            
            $remeber_me=null;
            if ($request->remeber_user==1) {
                $token->expires_at = Carbon::now()->addWeeks(1);
                $remeber_me=1;
            }else{
                $remeber_me=0;
            }
            $token->save();
       
        return response()->json([
            'status' => 'success',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'code'=>200,
            'password'=>$request->password,
            'remeber'=>$request->remeber_user,
            'data'=>$user,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
}