<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;
use App\Models\TeamComposition;

class TeamController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $teams=TeamComposition::where('type',0)->get();
       return $this->sendResponse($teams,'Get List Successfuly');
    }

    public function getTeamByIdUser(Request $request, $id){
        $search_text=$request->query('search_text');
        $teams= TeamComposition::orderBy('id','desc')
        ->with(['champions.classes.combinationRates','champions.advancedItems.base_items','champions.advancedItems.classes'])
        ->where('user_id',$id)->where('name', 'LIKE', "%{$search_text}%")->get();
        return $this->sendResponse($teams,'Get List Successfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $team = new TeamComposition([
            'name'=>$request->name,
            'rated'=>$request->rated,
            'user_id'=>$request->user_id,
            'description'=>$request->description,
            'type'=>0,
        ]);
        $team->save();
        $team->champions()->attach($request->champions);
        return $this->sendResponse($team,'Add Team Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team=TeamComposition::where('id',$id)->with(['champions.classes.combinationRates'])->first();
        return $this->sendResponse($team,'Get Info By Id Successfuly');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, $id)
    {
        $team=TeamComposition::findOrFail($id);
        $team->champions()->detach();
        $team->name=$request->name;
        $team->rated=$request->rated;
        $team->user_id=$request->user_id;
        $team->description=$request->description;
        $team->type=0;
        $team->save();
        $team->champions()->attach($request->champions);
        return $this->sendResponse($team, 'Update Team Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team=TeamComposition::findOrFail($id);
        $team->delete();
        return $this->sendResponse($team,'Delete Successfuly');
    }
}
