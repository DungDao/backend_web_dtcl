<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Champion;
use App\Models\AdvancedItem;
use App\Models\TeamComposition;
use App\Models\BaseItem;
use App\Models\Classes;
use App\Models\Utility;
use App\Models\RollingOdd;
use App\Models\CommentTeam;
use App\User;
use DB;
use Validator;
class HomeController extends BaseController
{
    public function getAllChampions(Request $request){
        $search_text=$request->query('search_text');
        $search_price=$request->query('search_price');
        $search_rated=$request->query('search_rated');

        $champions=Champion::orderBy('price','desc')
	    	->where([['price', 'Like',"%{$search_price}%"],['rated', 'like',"%{$search_rated}%"]])
	    	->where('name', 'like',"%{$search_text}%")
           	->with(['classes','advancedItems'])->get();
        return $this->sendResponse($champions,'Get List Champions Successfuly');
    }

    public function getChampionById($id){
    	$champion=Champion::where('id',$id)->with(['classes.combinationRates', 'classes.champions.advancedItems','classes.champions.classes','advancedItems.base_items'])->first();
    	return $this->sendResponse($champion,'Show Success');
    }

    public function getChampionByRated(){
        $champions= Champion::groupBy('rated')->get();
        return $this->sendResponse($champions,'Get successfylu');
    }

    public function getChampions(Request $request){
        $search_text=$request->query('search_text');
        $champions=Champion::orderBy('price','DESC')
            ->where('name', 'LIKE', "%{$search_text}%")
            ->orWhere('price','LIKE', "%{$search_text}%")
            ->with(['classes.combinationRates','teams','advancedItems'])->get();
        return $this->sendResponse($champions,'Get List Champions Cuccessfuly');
    }

    // Items

    public function getAllItems(Request $request){
        $search_text=$request->query('search_text');
    	$items=AdvancedItem::orderBy('name','asc')->where('name', 'LIKE', "%{$search_text}%")
            ->orWhere('rated','LIKE', "%{$search_text}%")
    	->with(['base_items','classes.combinationRates'])->get();
    	return $this->sendResponse($items,'Get List Items Successfuly');
    }

    public function getAllBaseItems(){
    	$items=BaseItem::orderBy('name','asc')
    	->with('advanced_items')->get();
    	return $this->sendResponse($items,'Successfuly');
    }

    public function getAllClass(){
    	$classes=Classes::orderBy('type','asc')
    	->with(['champions','combinationRates'])->get();
    	return $this->sendResponse($classes,'get class successfuly');
    }

     public function getAllClassWithChampions(Request $request){
        $search_text=$request->query('search_text');
        $classes=Classes::orderBy('type','asc')->where('name', 'LIKE', "%{$search_text}%")
        ->with(['champions.classes','champions.advancedItems','combinationRates'])->get();
        return $this->sendResponse($classes,'get class successfuly');
    }

    public function getTeams(Request $request){
        $search_text=$request->query('search_text');
        $teams= TeamComposition::orderBy('rated','asc')->where('type',1)->where('name', 'LIKE', "%{$search_text}%")->with(['champions.classes.combinationRates','champions.advancedItems.base_items'])->get();
        return $this->sendResponse($teams,'Get List Successfuly');
    }

    // Utilities

    public function getByTypeUtitlitis(Request $request){
        $type=$request->query('type');
        $check='desc';
        if($type==0){
            $check=$check;
        }else{
            $check='asc';
        }
        $utilities= Utility::orderBy('value',$check)->where('type','=',$type)->get();
        return $this->sendResponse($utilities,'Get Successfuly');
    }

    public function getRollings()
    {
        $rollings=RollingOdd::orderBy('level','asc')->get();
        return $this->sendResponse($rollings,' Get List Successfuly');
    }

    public function getUserById($id){
        $user=User::findOrFail($id);
        return $this->sendResponse($user,'get User Successfuly');
    }

    public function changInfoUser(Request $request, $id){
        $user=User::findOrFail($id);
        $user->name=$request->name;
        $user->avatar=$request->avatar;
        $user->email=$request->email;
        $user->phone=$request->phone;
        if($request->password=='old_password'){
            $user->password=$user->password;
        }else{
             $user->password=bcrypt($request->password);    
        }
        $user->roles=0;
        $user->save();

        return $this->sendResponse($user,'Change Info User Successfuly');
    }
    
     public function addCommentTeams(Request $request){
        $comment=new CommentTeam([
            'user_id'=>$request->user_id,
            'team_id'=>$request->team_id,
            'danhgia'=>$request->danhgia,
        ]);
        $comment->save();
        return $this->sendResponse($comment,'Add Successffuly');
    }

    
}
