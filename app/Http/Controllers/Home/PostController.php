<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\CategoryPost;
class PostController extends BaseController
{
    public function getPostByCategory($id){
    	$posts=Post::orderBy('created_at','desc')->with(['category'])
        ->where('category_id',$id)->where('status',1)->get();

        return $this->sendResponse($posts,'Get Successfuly');
    }

    public function getAllCategory(){
    	$category=CategoryPost::orderBy('id','asc')->with(['posts' => function ($query) {
            $query->where('status',1)->orderBy('created_at','desc');
        }])->withCount('posts')->limit(5)->get();
    	return $this->sendResponse($category,' get Successfuly');
    }

    public function getPostNew(){
    	$posts=Post::orderBy('created_at','desc')->with(['category'])->where('status',1)->limit(10)->get();

        return $this->sendResponse($posts,'Get Successfuly');
    }

    public function getPostById($id){
    	$posts=Post::orderBy('created_at','desc')->with(['category','comments.user'])->where('status',1)->where('id',$id)->get();

        return $this->sendResponse($posts,'Get Successfuly');
    }

    public function getPostPopu(){
    	$posts=Post::orderBy('view_number','desc')->with(['category'])->where('status',1)->limit(9)->get();

        return $this->sendResponse($posts,'Get Successfuly');
    }

     public function watchPost($id){
        $post=Post::findOrFail($id);
        $post->view_number=$post->view_number+1;
        $post->save();

        return $this->sendResponse($post,'Add Successfuly');
    }

}


