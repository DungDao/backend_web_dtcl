<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\BaseOfAdvancedItem;
use App\Http\Requests\BaseOfAdvancedItemRequest;

class BaseOfAvancedItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $base_of_advanced_items=BaseOfAdvancedItem::all();
        return $this->sendResponse($base_of_advanced_items,'Show List Successfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BaseOfAdvancedItemRequest $request)
    {
        $base_of_advanced_item= new BaseOfAdvancedItem([
            'advanced_item_id'=>$request->advanced_item_id,
            'base_item_id'=>$request->base_item_id
        ]);

        $base_of_advanced_item->save();
        return $this->sendResponse($base_of_advanced_item,'Add successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BaseOfAdvancedItemRequest $request, $id)
    {
        $base_of_advanced_item=BaseOfAdvancedItem::findOrFail($id);
        $base_of_advanced_item->advanced_item_id=$request->advanced_item_id;
        $base_of_advanced_item->base_item_id=$request->base_item_id;
        $base_of_advanced_item->save();
        return $this->sendResponse($base_of_advanced_item,'Update Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $base_of_advanced_item=BaseOfAdvancedItem::findOrFail($id);
        $base_of_advanced_item->delete();
        return $this->sendResponse($base_of_advanced_item,'Delete Successfuly');
    }
}
