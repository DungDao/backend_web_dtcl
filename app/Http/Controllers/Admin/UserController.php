<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\BaseController;

class UserController extends BaseController
{
    public function getUsers(){
    	$users=User::orderBy('id','desc')->where('roles','=','0')
    	->withCount(['teams','comments'=> function ($query) {
            $query->where('type', '=', 0);
        },'comments AS like_count'=> function ($query) {
            $query->where('type', '=', 1);
        }])->get();
    	return $this->sendResponse($users,'Get List User Successfuly');
    }

    public function blockUser(Request $request, $id){
	 	$user=User::findOrFail($id);
        $user->block_status=$request->block_status;
        $user->save();
        return $this->sendResponse($user,'Change Status Successfuly');
    }
}
