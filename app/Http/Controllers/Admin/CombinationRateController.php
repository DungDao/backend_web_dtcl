<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Validator;
use App\Models\CombinationRate;

class CombinationRateController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combination_rates=CombinationRate::orderBy('id','DESC')->with('classe')->paginate(10);

        return $this->sendResponse($combination_rates,'Show list CombinationRate paginate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'classes_id' => 'required',
            'combination' => 'required',
            'content'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

         $combination_rate= new CombinationRate([
            'classes_id'=>$request->classes_id,
            'combination'=>$request->combination,
            'content'=>$request->content,
         ]);

         $combination_rate->save();
         return $this->sendResponse($combination_rate,'Add Combination rate successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $combination_rate=CombinationRate::findOrFail($id);
        return $this->sendResponse($combination_rate,'Info of Combination rate');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'classes_id' => 'required',
            'combination' => 'required',
            'content'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

         $combination_rate=CombinationRate::findOrFail($id);
         $combination_rate->classes_id=$request->classes_id;
         $combination_rate->combination=$request->combination;
         $combination_rate->content=$request->content;
         $combination_rate->save();

         return $this->sendResponse($combination_rate, 'Update combination rate successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $combination_rate=CombinationRate::findOrFail($id);
        $combination_rate->delete();
        return $this->sendResponse($combination_rate,'Delete Combination Rate Successfuly');
    }
}
