<?php

namespace App\Http\Controllers\Admin;
use App\Models\Classes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Http\Resources\ClassesResource;
use App\Http\Controllers\BaseController as BaseController;
use Validator;
use DB;

class ClassController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_text=$request->query('search_text');
        $classes=Classes::orderBy('id','DESC')->where('name', 'LIKE', "%{$search_text}%")
            ->orWhere('skill', 'LIKE', "%{$search_text}%")->withCount('champions')
            ->with(['combinationRates'])->get();
        return $this->sendResponse($classes,'Get Classes Cuccessfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:classes',
            'avatar' => 'required',
            'skill' => 'required',
            'rated'=>'required',
            'type'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

        $classes = new Classes([
            'name' => $request->name,
            'avatar' => $request->avatar,
            'skill' => $request->skill,
            'rated'=>$request->rated,
            'type'=>$request->type,
            'info_update'=>$request->info_update,
        ]);

        $classes->save();

        return $this->sendResponse($classes,"Add Classes Cuccessfuly");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classes=Classes::find($id);

        return $this->sendResponse($classes, 'show info classes success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'avatar' => 'required',
            'skill' => 'required',
            'rated'=>'required',
            'type'=>"required"
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

         $classes= Classes::find($id);
         $classes->name=$request->name;
         $classes->avatar=$request->avatar;
         $classes->skill= $request->skill;
         $classes->rated=$request->rated;
         $classes->type=$request->type;
         $classes->info_update=$request->info_update;

         $classes->save();
         return $this->sendResponse($classes,"Edit Classes Cuccessfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classes= Classes::findOrFail($id);
        $classes->delete();
        return $this->sendResponse($classes,"Delete Cucessfuly");
    }

    public function getAll(){
        $classes=Classes::all();
        return $this->sendResponse($classes,'Get All Classes Successfuly');
    }
}
