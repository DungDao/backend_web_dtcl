<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\CategoryPost;
use Validator;
class CategoryPostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   $search_text=$request->query('search_text');
        $categories=CategoryPost::orderBy('id','ASC')
        ->where('name_category', 'LIKE', "%{$search_text}%")
        ->orWhere('description', 'LIKE', "%{$search_text}%")
        ->with('posts')->get();

        return $this->sendResponse($categories,'Show list Category paginate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_category' => 'required|string|unique:category_post',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

         $category= new CategoryPost([
            'name_category'=>$request->name_category,
            'description'=>$request->description
         ]);

         $category->save();
         return $this->sendResponse($category,'Add category successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=CategoryPost::findOrFail($id);
        return $this->sendResponse($category,'Info of Category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name_category' => 'required|string',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

         $category=CategoryPost::findOrFail($id);
         $category->name_category=$request->name_category;
         $category->description=$request->description;
         $category->save();

         return $this->sendResponse($category, 'Update category successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=CategoryPost::findOrFail($id);
        $category->delete();
        return $this->sendResponse($category,'Delete Category Successfuly');
    }
    public function getAll(){
        $categories=CategoryPost::all();
        return $this->sendResponse($categories,'Get All Successfuly');
    }
}
