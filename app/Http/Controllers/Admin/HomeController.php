<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\Champion;
use App\Models\Post;
use App\User;
use App\Models\Comment;
use DB;
class HomeController extends BaseController
{
    public function getTotalInTable(){
    	$totalChampion=Champion::count();
    	$totalUsers=DB::table('users')->where('roles',0)->count();
    	$totalPost=Post::count();
    	$totalTeam=DB::table('team_composition')->where('type',1)->count();
    	$obj=(object)[];
    		$obj->total_champion=$totalChampion;
    		$obj->total_post=$totalPost;
    		$obj->total_team=$totalTeam;
    		$obj->total_user=$totalUsers;

    	return $this->sendResponse($obj, 'Get Successfuly');
    }

    public function getUserByCreateAt(){
    	$result = User::selectRaw('year(created_at) year, month(created_at) month, count(*) number_users')
    			->where('roles',0)
                ->groupBy('year', 'month')
                ->orderBy('year', 'asc')->limit(12)
                ->get();

         return $this->sendResponse($result,'Get list user by motnth of year');
    }

    public function getNumberPostByDate(){
    	$result = Post::selectRaw('year(created_at) year, month(created_at) month, count(*) number_posts')
    			->where('status',1)
                ->groupBy('year', 'month')
                ->orderBy('year', 'asc')->limit(12)
                ->get();

         return $this->sendResponse($result,'Get list user by motnth of year');
    }
}
