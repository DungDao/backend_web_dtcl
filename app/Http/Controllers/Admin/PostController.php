<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Post;
use Validator;

class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $search_text=$request->query('search_text');
        $posts=Post::orderBy('id','DESC')->where('title', 'LIKE', "%{$search_text}%")->with(['category'])->withCount(['comments'=> function ($query) {
            $query->where('type', '=', 0);
        },'comments AS like_count'=> function ($query) {
            $query->where('type', '=', 1);
        }])->get();

        return $this->sendResponse($posts,'get list post success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required|unique:table_post',
            'description'=>'required',
            'status'=>'required',
            'avatar'=>'required',
            'content'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }
        
        $post = new Post([
            'category_id' => $request->category_id,
            'title' => $request->title,
            'description' => $request->description,
            'status'=>$request->status,
            'content'=>$request->content,
            'avatar'=>$request->avatar
        ]);
 
        $post->save();
 
        return $this->sendResponse($post,"Add Post Cuccessfuly");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::find($id);
        if($post!=null){
            return $this->sendResponse($post,'info post');
        }else{
            return response()->json([
                'code'=>404,
                'message'=>'Id Post doest not exists'

            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
            'description'=>'required',
            'status'=>'required',
            'content'=>'required',
            'avatar'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

         $post=Post::findOrFail($id);
         $post->category_id=$request->category_id;
         $post->title=$request->title;
         $post->description=$request->description;
         $post->status=$request->status;
         $post->avatar=$request->avatar;
         $post->content=$request->content;

         $post->save();

         return $this->sendResponse($post,'update post success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=Post::findOrFail($id);
        $post->delete();
        return $this->sendResponse($post,'Delete Cucess');
    }

    public function posted($id,Request $request){
            $post= Post::findOrFail($id);
            $post->status=$request->status;
            $post->save();

            return $this->sendResponse($post,'Posted Successfuly');
    }

    public function getPostByCategory($id, Request $request){
         $search_text=$request->query('search_text');
        $posts=Post::orderBy('id','DESC')->where('category_id',$id)->where('title', 'LIKE', "%{$search_text}%")->with(['category'])->withCount(['comments'=> function ($query) {
            $query->where('type', '=', 0);
        },'comments AS like_count'=> function ($query) {
            $query->where('type', '=', 1);
        }])->get();

        return $this->sendResponse($posts,'get Successfuly');
    }
}
