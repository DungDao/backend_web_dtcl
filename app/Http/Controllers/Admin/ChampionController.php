<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\Champion;
use App\Http\Requests\ChampionRequest;
use Validator;

class ChampionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $search_text=$request->query('search_text');
        $champions=Champion::orderBy('price','DESC')
            ->where('name', 'LIKE', "%{$search_text}%")
            ->orWhere('rated','LIKE', "%{$search_text}%")
            ->orWhere('price','LIKE', "%{$search_text}%")
            ->with(['classes.combinationRates','teams','advancedItems'])->get();
        return $this->sendResponse($champions,'Get List Champions Cuccessfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChampionRequest $request)
    {
        $champion = new Champion([
            'name' => $request->name,
            'avatar' => $request->avatar,
            'mana' => $request->mana,
            'price'=>$request->price,
            'health'=>$request->health,
            'start_mana'=>$request->start_mana,
            'range'=>$request->range,
            'attack_damage'=>$request->attack_damage,
            'attack_speed'=>$request->attack_speed,
            'armor'=>$request->armor,
            'dps'=>$request->dps,
            'magic_resist'=>$request->magic_resist,
            'skill'=>$request->skill,
            'avatar_skill'=>$request->avatar_skill,
            'rated'=>$request->rated,
            'status'=>$request->status,
            'experience'=>$request->experience,
            'info_update'=>$request->info_update,
            'info_skill'=>$request->info_skill
        ]);
 
        $champion->save();
        $champion->classes()->attach($request->classes);
        $champion->advancedItems()->attach($request->items);
        return $this->sendResponse($champion,"Add Champion Cuccessfuly");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $champion=Champion::findOrFail($id);

        return $this->sendResponse($champion, 'show info champion success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChampionRequest $request, $id)
    {

         $champion= Champion::find($id);
         $champion->classes()->detach();
         $champion->advancedItems()->detach();
         $champion->name = $request->name;
         $champion->avatar = $request->avatar;
         $champion->mana = $request->mana;
         $champion->price=$request->price;
         $champion->health=$request->health;
         $champion->start_mana=$request->start_mana;
         $champion->range=$request->range;
         $champion->attack_damage=$request->attack_damage;
         $champion->attack_speed=$request->attack_speed;
         $champion->armor=$request->armor;
         $champion->dps=$request->dps;
         $champion->magic_resist=$request->magic_resist;
         $champion->skill=$request->skill;
         $champion->avatar_skill=$request->avatar_skill;
         $champion->rated=$request->rated;
         $champion->status=$request->status;
         $champion->experience=$request->experience;
         $champion->info_update=$request->info_update;
         $champion->info_skill=$request->info_skill;

         $champion->save();

        $champion->classes()->attach($request->classes);
        $champion->advancedItems()->attach($request->items);
         return $this->sendResponse($champion,"Edit Champion Cuccessfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $champion= Champion::findOrFail($id);
        $champion->delete();
        return $this->sendResponse($champion,"Delete Champion Cucessfuly");
    }

    public function getAll(){
        $champions=Champion::all();
        return $this->sendResponse($champions,"Get all successfuly");
    }
}
