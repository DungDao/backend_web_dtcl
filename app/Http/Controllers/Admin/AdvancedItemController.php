<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\AdvancedItem;
use App\Http\Requests\AdvancedItemRequest;

class AdvancedItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_text=$request->query('search_text');
        // $advanced_items= AdvancedItem::all();
        $advanced_items= AdvancedItem::orderBy('id','desc')->where('name', 'LIKE', "%{$search_text}%")
            ->orWhere('uses', 'LIKE', "%{$search_text}%")->orWhere('info_update', 'LIKE', "%{$search_text}%")->orWhere('rated','LIKE', "%{$search_text}%")->with('base_items','classes.combinationRates')->get();
        return $this->sendResponse($advanced_items,'Get List Advanced Item ');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvancedItemRequest $request)
    {
        $advanced_item=new AdvancedItem([
            'name'=>$request->name,
            'avatar'=>$request->avatar,
            'uses'=>$request->uses,
            'rated'=>$request->rated,
            'class_id'=>$request->class_id,
            'info_update'=>$request->info_update
        ]);
        $advanced_item->save();

        $advanced_item->base_items()->attach($request->base_item_id1);
        $advanced_item->base_items()->attach($request->base_item_id2);

        return $this->sendResponse($advanced_item,'Add Advanced Item Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advanced_item=AdvancedItem::findOrFail($id);
        return $this->sendResponse($advanced_item,'Show info Advanced Item Successfuly');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvancedItemRequest $request, $id)
    {
        $advanced_item=AdvancedItem::findOrFail($id);
        $advanced_item->base_items()->detach();
        $advanced_item->avatar=$request->avatar;
        $advanced_item->name=$request->name;
        $advanced_item->uses=$request->uses;
        $advanced_item->rated=$request->rated;
        $advanced_item->class_id=$request->class_id;
        $advanced_item->info_update=$request->info_update;
        $advanced_item->save();

        $advanced_item->base_items()->attach($request->base_item_id1);
        $advanced_item->base_items()->attach($request->base_item_id2);
        return $this->sendResponse($advanced_item,'Update Advanced Item Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advanced_item=AdvancedItem::findOrFail($id);
        $advanced_item->base_items()->detach();
        $advanced_item->delete();
        return $this->sendResponse($advanced_item,'Delete Advanced Item Successfuly');
    }

    public function getAll(){
        $advanced_items= AdvancedItem::orderBy('id','desc')->with('base_items')->get();
        return $this->sendResponse($advanced_items,'Get All Advanced Item Successfuly');
    }
}
