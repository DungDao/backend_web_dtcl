<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\BaseItem;
use App\Http\Requests\BaseItemRequest;

class BaseItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $search_text=$request->query('search_text');
        $base_items=BaseItem::orderBy('id','desc')->where('name', 'LIKE', "%{$search_text}%")
            ->orWhere('uses_type', 'LIKE', "%{$search_text}%")->orWhere('info_update', 'LIKE', "%{$search_text}%")->withCount('advanced_items')->get();
        return $this->sendResponse($base_items,'Get List Base Item Successfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BaseItemRequest $request)
    {
        $base_item = new BaseItem([
            'name'=>$request->name,
            'avatar'=>$request->avatar,
            'uses_type'=>$request->uses_type,
            'number_power'=>$request->number_power,
            'rated'=>$request->rated,
            'info_update'=>$request->info_update
        ]);
        $base_item->save();
        return $this->sendResponse($base_item,'Add Base Item Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $base_item=BaseItem::findOrFail($id);
        return $this->sendResponse($base_item,'show info Base item successfuly');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BaseItemRequest $request, $id)
    {
        $base_item=BaseItem::findOrFail($id);
        $base_item->name=$request->name;
        $base_item->avatar=$request->avatar;
        $base_item->uses_type=$request->uses_type;
        $base_item->rated=$request->rated;
        $base_item->number_power=$request->number_power;
        $base_item->info_update=$request->info_update;
        $base_item->save();
        return $this->sendResponse($base_item,'Update Base Item Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $base_item=BaseItem::findOrFail($id);
        $base_item->delete();
        return $this->sendResponse($base_item,'Delete Base Item Successfuly');
    }

    public function getAll(){
        $base_items=BaseItem::all();
        return $this->sendResponse($base_items,'Get All Base Item');
    }
}
