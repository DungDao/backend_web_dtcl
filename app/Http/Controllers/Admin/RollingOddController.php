<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\RollingOdd;
use App\Http\Requests\RollingRequest;
class RollingOddController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rollings=RollingOdd::orderBy('level','asc')->get();
        return $this->sendResponse($rollings,' Get List Successfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RollingRequest $request)
    {
        $rolling= new RollingOdd ([
            'level'=>$request->level,
            'tier1'=>$request->tier1,
            'tier2'=>$request->tier2,
            'tier3'=>$request->tier3,
            'tier4'=>$request->tier4,
            'tier5'=>$request->tier5,
        ]);
        $rolling->save();
        return $this->sendResponse($rolling,'Add Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rolling=RollingOdd::findOrFail($id);
        return $this->sendResponse($rolling,'Show Rolling by Id');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RollingRequest $request, $id)
    {
        $rolling=RollingOdd::findOrFail($id);
        $rolling->level=$request->level;
        $rolling->tier1=$request->tier1;
        $rolling->tier2=$request->tier2;
        $rolling->tier3=$request->tier3;
        $rolling->tier4=$request->tier4;
        $rolling->tier5=$request->tier5;
        $rolling->save();
        return $this->sendResponse($rolling,'Update Rolling Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rolling=RollingOdd::findOrFail($id);
        $rolling->delete();
        return $this->sendResponse($rolling,'Delete Rolling Successfuly');
    }
}
