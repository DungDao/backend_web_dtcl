<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Utility;
use App\Http\Requests\UtilitiesRequest;
class UtilityController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utilities=Utility::orderBy('value','asc')->get();
        return $this->sendResponse($utilities,'Get List Successfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UtilitiesRequest $request)
    {
        $utility=new Utility([
            'name'=>$request->name,
            'value'=>$request->value,
            'type'=>$request->type,
            'info_update'=>$request->info_update
        ]);

        $utility->save();
        return $this->sendResponse($utility,'Create Utility Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $utility=Utility::findOrFail($id);
        return $this->sendResponse($utility,'Info Utinity by Id');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UtilitiesRequest $request, $id)
    {
        $utility=Utility::findOrFail($id);
        $utility->name=$request->name;
        $utility->value=$request->value;
        $utility->type=$request->type;
        $utility->info_update=$request->info_update;
        $utility->save();
        return $this->sendResponse($utility,'Update Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $utility=Utility::findOrFail($id);
        $utility->delete();
        return $this->sendResponse($utility,'Delete Utility Successfuly');
    }
    public function getByType(Request $request){
        $type=$request->query('type');
        $check='desc';
        if($type==0){
            $check=$check;
        }else{
            $check='asc';
        }
        $utilities= Utility::orderBy('value',$check)->where('type','=',$type)->get();
        return $this->sendResponse($utilities,'Get Successfuly');
    }
}
