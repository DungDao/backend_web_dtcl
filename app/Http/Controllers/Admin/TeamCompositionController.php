<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;
use App\Models\TeamComposition;
use App\Models\CommentTeam;

class TeamCompositionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams= TeamComposition::orderBy('id','desc')
        ->with(['champions.classes','comments'])->withCount(['comments'])->where('type',1)->get();
        return $this->sendResponse($teams,'Get List Successfuly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $team = new TeamComposition([
            'name'=>$request->name,
            'rated'=>$request->rated,
            'user_id'=>$request->user_id,
            'description'=>$request->description,
            'type'=>1,
        ]);
        $team->save();
        $team->champions()->attach($request->champions);
        return $this->sendResponse($team,'Add Team Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team=TeamComposition::where('id',$id)->with(['champions.classes.combinationRates'])->first();
        return $this->sendResponse($team,'Get Info By Id Successfuly');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, $id)
    {
        $team=TeamComposition::findOrFail($id);
        $team->champions()->detach();
        $team->name=$request->name;
        $team->rated=$request->rated;
        $team->user_id=$request->user_id;
        $team->description=$request->description;
        $team->type=1;
        $team->save();
        $team->champions()->attach($request->champions);
        return $this->sendResponse($team, 'Update Team Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team=TeamComposition::findOrFail($id);
        $team->delete();
        // $team->champions()->detach()
        return $this->sendResponse($team,'Delete Successfuly');
    }

    public function addCommentTeams(Request $request){
        $comment=new CommentTeam([
            'user_id'=>$request->user_id,
            'team_id'=>$request->team_id,
            'danhgia'=>$request->danhgia,
        ]);
        $comment->save();
        return $this->sendResponse($comment,'Add Successffuly');
    }
}
