<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Mail\ForgotPassword as ForgotPasswordMail;
use Illuminate\Support\Facades\Mail;
use App\Notifications\ResetPasswordRequest;

class ResetPasswordController extends Controller
{
    /**
     * Create token password reset.
     *
     * @param  ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function sendMail(Request $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();

        if (!$user)
            return response()->json([
                'message' => 'Chúng tôi không tìm thấy tài khoản nào hợp lệ'
            ], 404);

        $passwordReset = PasswordReset::updateOrCreate(
            [
                'email' => $user->email,
                'token' => Str::random(60)
             ]
         );

        if ($user&&$passwordReset) {
            Mail::to($passwordReset->email)->send( new ForgotPasswordMail(
            	[
            		'name'=>$user->name,
            		'link'=>url('http://localhost:3000/users/reset-password/'.$passwordReset->token)
            	])
        	);
        }
  
        return response()->json([
        'message' => 'Chúng tôi đã gửi qua Email đường dẫn đặt lại mật khẩu của bạn!',
        'code'=>200
        ]);
    }

    public function reset(Request $request, $token)
    {
        $passwordReset = PasswordReset::where('token', $token)->firstOrFail();
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();

            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 422);
        }
        $user = User::where('email', $passwordReset->email)->firstOrFail();
        // $updatePasswordUser = $user->update($request->only('password'));
        if($user !=null){
            $user->password= bcrypt($request->password);
            $user->save();
        }
        $passwordReset->delete();

        return response()->json([
            'success' => $user,
            'code'=>200
        ]);
    }
}