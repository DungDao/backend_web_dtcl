<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class FileController extends BaseController
{
    public function upload(Request $request){

        $validator = Validator::make($request->all(), [
            'files' => 'array',
            'files.*' => 'file',
            // 'type' => 'image|required'
        ]);
 
         if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

        if ($request->hasFile('image')) {
            $filename =rand(11111, 99999) .$request->file('image')->getClientOriginalName();
            $request->file('image')->move('uploads/images/',$filename);
            return $this->sendResponse($filename,'add image successfuly');
        }
    }

    public function uploadUrlEditor(Request $request){

        $validator = Validator::make($request->all(), [
            'files' => 'array',
            'files.*' => 'file'
        ]);
 
         if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors(),
            ]);
         }

        if ($request->hasFile('image')) {
            $filename =rand(11111, 99999) .$request->file('image')->getClientOriginalName();
            $request->file('image')->move('uploads/images/',$filename);
            $photoURL= url('uploads/images/'.$filename);

            return $this->sendResponse($photoURL,'add image successfuly');
        }
    }
}
