<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentTeam extends Model
{
    protected $table = 'table_comment_teams';

    protected $fillable = ['team_id','user_id','danhgia'];
    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
