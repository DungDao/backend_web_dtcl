<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseItem extends Model
{
    protected $table = 'base_items';

    protected $fillable = ['name','avatar','uses_type','number_power','rated','info_update'];

    protected $hidden = ['created_at', 'updated_at'];

    public function advanced_items(){
        return $this->belongsToMany('App\Models\AdvancedItem','base_of_advanced_item','base_item_id','advanced_item_id')->withPivot('id', 'advanced_item_id','base_item_id');
    }
}
