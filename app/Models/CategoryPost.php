<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model
{
    protected $table = 'category_post';

    protected $fillable = ['name_category','description'];

    protected $hidden = ['created_at', 'updated_at'];

    public function posts(){
        return $this->hasMany('App\Models\Post', 'category_id', 'id');
    }
}
