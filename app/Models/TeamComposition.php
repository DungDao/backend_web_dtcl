<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamComposition extends Model
{
    protected $table = 'team_composition';

    protected $fillable = ['name','rated','description','type','user_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function champions(){
        return $this->belongsToMany('App\Models\Champion','champions_of_team','team_composition_id','champion_id')->withPivot('id','three_stars','position', 'item1','item2','item3');
    }

    public function items(){
        return $this->belongsToMany('App\Models\AdvancedItem','champions_of_team','team_composition_id','item1')->withPivot('id','three_stars','position', 'item1','item2','item3');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function comments(){
        return $this->hasMany('App\Models\CommentTeam','team_id','id');
    }

}
