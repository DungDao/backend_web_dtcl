<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CombinationRate extends Model
{
    protected $table = 'combination_rate';

    protected $fillable = ['classes_id','combination','content'];

    protected $hidden = ['created_at', 'updated_at'];

    public function classe(){
        return $this->belongsTo('App\Models\Classes', 'classes_id', 'id');
    }
}
