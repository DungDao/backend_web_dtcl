<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Utility extends Model
{
    protected $table = 'utilities';

    protected $fillable = ['name','value','type','info_update'];

    protected $hidden = ['created_at', 'updated_at'];
}
