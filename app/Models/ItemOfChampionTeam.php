<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemOfChampionTeam extends Model
{
    protected $table = 'items_of_champion_team';

    protected $fillable = ['item_id','champion_id'];

    protected $hidden = ['created_at', 'updated_at'];
}
