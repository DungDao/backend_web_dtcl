<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    protected $table = 'champions';

    protected $fillable = ['name','avatar','mana','price','health',
        'start_mana','range','attack_damage','attack_speed','armor',
        'dps','magic_resist','skill','avatar_skill','rated','status','experience','info_skill'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function classes(){
        return $this->belongsToMany('App\Models\Classes','champions_by_classes','champion_id','classes_id')->withPivot('id');
    }
    public function teams(){
        return $this->belongsToMany('App\Models\TeamComposition','champions_of_team','champion_id','team_composition_id')->withPivot('id');
    }

     public function advancedItems(){
        return $this->belongsToMany('App\Models\AdvancedItem','items_of_chamions','champion_id','item_id')->withPivot('id');
    }
}
