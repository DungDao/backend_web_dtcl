<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';

    protected $fillable = ['name','avatar','skill','rated','combinationRates','info_update'];

    protected $hidden = ['created_at', 'updated_at'];

    public function combinationRates(){
        return $this->hasMany('App\Models\CombinationRate', 'classes_id', 'id');
    }

    public function champions(){
        return $this->belongsToMany('App\Models\Champion','champions_by_classes','classes_id','champion_id')->withPivot('id', 'champion_id','classes_id');
    }
}
