<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'table_post';

    protected $fillable = ['category_id','title','description','status','view_number','avatar','content','created_at'];
    protected $appends =['numberOfComments','numberOfLikes'];

    protected $hidden = ['updated_at'];

    public function category(){
        return $this->belongsTo('App\Models\CategoryPost', 'category_id', 'id');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment','post_id','id');
    }

    public function getNumberOfCommentsAttribute(){
    	return $this->comments->where('type','0')->count();
    }
    public function getNumberOfLikesAttribute(){
    	return $this->comments->where('type','1')->count();
    }
}
