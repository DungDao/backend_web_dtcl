<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChampionByClass extends Model
{
    protected $table = 'champions_by_classes';

    protected $fillable = ['champion_id','classes_id'];

    protected $hidden = ['created_at', 'updated_at'];

    // public function combinationRates(){
    //     return $this->hasMany('App\Models\CombinationRate', 'classes_id', 'id');
    // }
}
