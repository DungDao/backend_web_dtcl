<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RollingOdd extends Model
{
    protected $table = 'rolling_odds';

    protected $fillable = ['level','tier1','tier2' ,'tier3' ,'tier4' ,'tier5'];
}
