<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = ['post_id','user_id','content','type','id_parent','created_at'];

    protected $hidden = ['updated_at'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
