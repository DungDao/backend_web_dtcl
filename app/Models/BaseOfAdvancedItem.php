<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseOfAdvancedItem extends Model
{
    protected $table = 'base_of_advanced_item';

    protected $fillable = ['advanced_item_id','base_item_id'];

    protected $hidden = ['created_at', 'updated_at'];
}
