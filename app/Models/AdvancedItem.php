<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvancedItem extends Model
{
    protected $table = 'advanced_items';

    protected $fillable = ['name','uses','avatar','rated','info_update', 'class_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function base_items(){
        return $this->belongsToMany('App\Models\BaseItem','base_of_advanced_item','advanced_item_id','base_item_id')->withPivot('id', 'advanced_item_id','base_item_id');
    }

    public function classes(){
    	return $this->belongsTo('App\Models\Classes','class_id','id');
    }
}
