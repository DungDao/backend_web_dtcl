<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChampionOfTeam extends Model
{
    protected $table = 'champions_of_team';

    protected $fillable = ['team_composition_id','champion_id','position','three_stars','item1', 'item2','item3'];

    protected $hidden = ['created_at', 'updated_at'];
}
