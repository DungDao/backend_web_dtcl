<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemOfChampion extends Model
{
    protected $table = 'items_of_chamions';

    protected $fillable = ['item_id','champion_id'];

    protected $hidden = ['created_at', 'updated_at'];

    // public function classe(){
    //     return $this->belongsTo('App\Models\Classes', 'classes_id', 'id');
    // }
}
